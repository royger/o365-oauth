# Office 365 OAuth token generator

Standalone Python script to generate OAuth tokens for Office 365.

## Intallation

None required, just run the script with an associated configuration file.

## Usage

Rename and edit the `example.json` configuration file to contain the data to
the service you are trying to generate a token for:

* `tenant_id`: part of the endpoint uri used to interact with the
  authentication service. There are both private and public values that can be
  used. See
  https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-v2-protocols#endpoints
  for more information.

* `client_id`: the application (client) ID. This is the ID assigned to the
  application trying to access the service. See 
  https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-auth-code-flow#request-an-authorization-code
  for more information.

* `redirect_uri`: the uri where the authentication response is sent. Native
  applications should use the default
  `https://login.microsoftonline.com/common/oauth2/nativeclient`.

* `scope`: permissions required by the application using the token are mapped
  to scopes. See
  https://docs.microsoft.com/en-us/exchange/client-developer/legacy-protocols/how-to-authenticate-an-imap-pop-smtp-application-by-using-oauth#configure-your-application
  for the scopes related to email clients for example. Note that the script will
  append the `offline_access` scope, so that the token can be refreshed.

Once the configuration file has been filled the script needs to be initialized
by using:

```
% ./o365-oauth -i config.json
```

It will give you an url to fetch a code from. You might have to input your
credentials in order to get the code. Note that the code requested will be
returned in the resulting url once the procedure has finished, and you will
have to copy it from the address bar. It's encoded as `...&code=<code>&...`.

After having given the code to the script you can now generate tokens using:

```
% ./o365-oauth config.json
```

## License

Apache License, Version 2.0
